<?php
require "animal.php";
require_once "Frog.php";
require_once "Ape.php";

$sheep = new Animal("shaun");
echo $sheep->name . "<br>"; // "shaun"
echo $sheep->legs . "<br>"; // 2
echo $sheep->cold_blooded . "<br>"; // false (tidak muncul karena jawabannya false)
echo "<br>";

$sungokong = new Ape("kera sakti");
echo $sungokong->name . "<br>";
echo $sungokong->yell . "<br>"; // "Auooo"
echo "<br>";

$kodok = new Frog("buduk");
echo $kodok->name . "<br>";
echo $kodok->frogLegs . "<br>";
echo $kodok->jump . "<br><br>"; // "hop hop"
echo "<br>";
